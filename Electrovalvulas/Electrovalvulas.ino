//Definicion de Alias para la prgramacion
//Definicion de botones(ENTRADAS)
#define INPUTAextendido 4
#define INPUTAretraido 5
#define INPUTBextendido 2
#define INPUTBretraido 3
#define Emergencia 6
//Definicion de salidas
#define Aextendido 7
#define Aretraido 8
#define Bextendido 9
#define Bretraido 10
#define LEDRojo 12
#define LEDVerde 11
//Bandera que sirve para guardar el estado anterior
bool estadoEmergencia = false;
bool estadoAnterior = false;

void setup() {
  //Definicion de las entradas en el microcontrolador
  pinMode(INPUTAextendido, INPUT);
  pinMode(INPUTAretraido, INPUT);
  pinMode(INPUTBextendido, INPUT);
  pinMode(INPUTBretraido, INPUT);
  pinMode(Emergencia, INPUT);
  //Definicion de salidas en el microcontrolador
  pinMode(Aextendido, OUTPUT);
  pinMode(Aretraido, OUTPUT);
  pinMode(Bextendido, OUTPUT);
  pinMode(Bretraido, OUTPUT);
  pinMode(LEDVerde, OUTPUT);
  pinMode(LEDRojo, OUTPUT);
  //Inicializacion de variables, todo se apaga al principio
  digitalWrite(Aextendido, LOW);
  digitalWrite(Aretraido, LOW);
  digitalWrite(Bextendido, LOW);
  digitalWrite(Bretraido, LOW);
  digitalWrite(LEDVerde, LOW);
  digitalWrite(LEDRojo, LOW);
  Serial.begin(9600);
  Serial.println("**********");
  Serial.println("Bienvenido");
  Serial.println("**********");
}

void loop() {
  if (!digitalRead(INPUTAextendido)) {
    Serial.println("Aplastado A+");
    digitalWrite(Aextendido, HIGH);
    digitalWrite(Aretraido, LOW);
    delay(50);
    if (!estadoEmergencia) digitalWrite(LEDVerde, HIGH);
  }
  else {
    digitalWrite(Aextendido, LOW);
  }

  if (!digitalRead(INPUTBextendido)) {
    delay(250);
    if (digitalRead(INPUTBextendido)) {
      delay(100);
      digitalWrite(Bextendido, HIGH);
      digitalWrite(Bretraido, LOW);
      Serial.println("Aplastado B+");
      estadoAnterior = HIGH;
      delay(500);
      if (!estadoEmergencia) digitalWrite(LEDVerde, HIGH);
    }
  }

  if (!digitalRead(INPUTAretraido)) {
    Serial.println("Aplastado A-");
    digitalWrite(Aextendido, LOW);
    digitalWrite(Aretraido, HIGH);
    if (!estadoEmergencia) digitalWrite(LEDVerde, HIGH);
    delay(10);
  }
  else {
    digitalWrite(Aretraido, LOW);
  }
  if (!digitalRead(INPUTBretraido)) {
    delay(250);
    if (digitalRead(INPUTBretraido)) {
      if (!estadoEmergencia) digitalWrite(LEDVerde, HIGH);
      delay(250);
      digitalWrite(Bextendido, LOW);
      digitalWrite(Bretraido, HIGH);
      Serial.println("Aplastado B-");
      estadoAnterior = LOW;
      delay(500);
    }
  }
  if (digitalRead(Emergencia) == true) {
    delay(25);
    if (digitalRead(Emergencia) == false) {
      delay(25);
      if (estadoEmergencia == false) {
        Serial.println("Emergencia = true");
        estadoEmergencia = true;
      }
      else {
        Serial.println("Emergencia = false");
        estadoEmergencia = false;
      }
      if (estadoEmergencia) {
        Serial.println("STOP");
        digitalWrite(LEDVerde, LOW);
        digitalWrite(LEDRojo, HIGH);
        //Este codigo está de cambiarle
        digitalWrite(Bextendido, LOW);
        digitalWrite(Bretraido, LOW);
        delay(250);
        ////////////////////////////////////
      }
      else {
        Serial.println("Continuar");
        digitalWrite(LEDVerde, HIGH);
        digitalWrite(LEDRojo, LOW);
        if (estadoAnterior) {
          digitalWrite(Bextendido, HIGH);
          digitalWrite(Bretraido, LOW);
        }
        else {
          digitalWrite(Bextendido, LOW);
          digitalWrite(Bretraido, HIGH);
        }
        delay(250);
      }
      Serial.println("Emergencia");
    }
  }
  delay(10);
}
